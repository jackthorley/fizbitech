FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /app

COPY api/service/service.csproj ./service/service.csproj
COPY api/web/api.csproj ./web/api.csproj
COPY sql/*.db ./sql/
RUN dotnet restore ./web/

COPY ./api ./release/

WORKDIR /app/release/web
RUN dotnet publish -c Release -o out


FROM build AS testrunner
WORKDIR /app/tests
COPY api/tests/. .
RUN ls .
ENTRYPOINT ["dotnet", "test", "./api.UnitTests"]


FROM microsoft/dotnet:2.1-aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=build /app/release/web/out ./
ENTRYPOINT ["dotnet", "api.dll"]