# Banking API

## Requirements

- Docker
- ASP.NET Core 2.1

## Build Docker Image

- Execute `build-release.sh`

## Running the site locally

- Execute `run.sh` and navigate to your localhost/Docker Machine IP (`docker-machine ip`) port `8000`

### HTTP methods

#### Get Users
##### Sample request

```
curl -I -X GET "https://localhost:8000/api/v1.0/users"
```

##### Sample response

```json
{
  "Users":[{
      "Id": 12345,
      "Firstname": "Jack",
      "Surname": "Thorley"
    },
    {
      "Id": 54321,
      "Firstname": "John",
      "Surname": "Thorley"
    }]
}
```

#### Create User

##### Parameters

| Parameter | Description | Data Type |
|-----------|------------|-----------|
| Body | The Create User information | CreateUserRequest |

##### DataTypes

**CreateUserRequest**:

| Parameter | Description | Data Type |
|-----------|-----------|-----------|
| Firstname | User's first name | string |
| Surname | User's surname | string |
| Account | The account you wish to associate with a user | Account |

**Account**:

| Parameter | Description | Data Type |
|-----------|-----------|-----------|
| BankName | Bank Identifier | string |
| AccountNumber | User's Account Number - **8 Characters, cannot start with 0** | string |

**Supported Bank Names**:

| Identifier |
|-----------|
| Fairway |
| Bizfibank |

##### Responses

_201 Created_: User has been created

_400 Bad Request_: Unknown non-critical error

_400 Bad Request_: Invalid Bank

_400 Bad Request_: Duplicate Account


## Running unit tests

- Execute `test.sh`

## Running Integration tests

- Execute `test-integration.sh`

--- 

# Thoughts and Notes

## API Responses

I typically try to return a message, along with a HTTP status to indicate to the consumer what has caused an issue or the payload.

### Unified Account + Transaction

To create the Unified response I would look into making a paged transaction resource and potentially add filters to the URL. HATEOAS (in this case JSON API), could be utilised to move from page, to page and enchance the resource. This would also allow people to code against the tags in the API for navigation allowing many benefits. Such as, URLs can be changed with a lower integration cost.

```json
{
  "links": {
    "self": "https://fairway.com/api/v1.0/accounts/123456/transactions",
    "next": "https://fairway.com/api/v1.0/accounts/123456/transactions?page[offset]=2",
    "last": "https://fairway.com/api/v1.0/accounts/123456/transactions?page[offset]=0",
  },
  "Account": {
    "AccountNumber": 123456,
    "SortCode": 31232,
    "links" : {
      "self": "https://fairway.com/api/v1.0/accounts/123456"
    }
  },
  "Transaction": [{
    "Date": "2017-08-12T20:17:46.384Z",
    "Ammount": 12.32,
    "Detail": "Wikos"
  },
  {
    "Date": "2017-08-13T08:17:46.384Z",
    "Ammount": 54.21,
    "Detail": "Boots"   
  }]
}
```

## DockerFiles & Docker Compose

I chose to invest some more time and research into implementing this process. Following some best practises defined by Docker I took some steps to segregated the Docker Images. The Unit tests use the base image which is compiled in the same DockerFile; as I feel Unit tests are an integral part of the build pipeline an Artifact cannot be considered completed without this base line. 

See branch `integration-test-framework`. This branch has taken some steps to seperated out Integration tests in an effort to provide quicker more direct feedback (It's important to note that the Integration tests use the base image still). This also means the test can be ran on any machine without the need for prerequisits. Isolating the need for version controlling assets, such as Dotnet core or the Databases and hence maintaining a more realistic testing environment.

eg. Using DockerCompose to inject environmental configuration to dictate the Database connection string & API Under test

```yaml 
version: '3'
services:
  api:    
    image: api    
    ports:
      - "8000:80"
  sut:   
    image: api/testing
    depends_on:
      - api
    environment:
      - ApiHost=http://api:8000
      - DBHost=http://mssql:1433
    command: dotnet test integrationTests.sln
  mssql:
      image: "microsoft/mssql-server-linux"
      env_file:
          - mssql/variables.env
      ports:
          - "1433:1433"
 ```

An additional change I would like to make would be to add configuration as part of environmental variables and template the configuration, this along with some other modifications, would help align to the 'Twelve Factor App' methodology.

## Shell Scripts

The Shell scripts are an integral part of keeping integrity amongst the various environments that this code might be executed. Currently the shell scripts purpose are really only for execution on a development machine/build agent. What it provides though is a source controlled, consistent method to build, test and run the application.

Ultimately it would be great to implement multiple Environment based DockerFiles and scripting (dev, staging, production) which could help to improve:
- Security - By removing the need for keys/passwords to be baked into environmental variables.
- Remove Fluff - Remove any extra DLLs/Configuration that are not required for a Production environment. This can also help with security.
- Logging/Debugging - Having different granularity and access/alerting to various parts of the systems

One caveat is that adding these changes could produce different artifacts/results for each environment.

## Connecting to external Banks

Given the banks are an external resource, it would be ideal to add as much resiliency to their transaction requests as possible. If they were to fall down, it would be ideal to hold that information (if permitted by regulations) temporarily. Or at least some top level information to give some enrichment.

### Multi-tenancy

It's clear from the brief that we're looking at a system that needs to be multi-tenanted hence why the database can accomodate for all banking partners. To expand on this I'd bake in the configuration away from the code, where possible, and add abstraction layers to manage the addition of new banks and their APIs. These abstraction layers would likely to be seperately releasable services communicating over message queues. That way a change in an API would not require a full release of the code base, it could also provide version controll of the messages.

### Weekly Data refresh

Batch jobs can be inheritenly painful, but sometimes a neccessity. Where possible I'd create an event sourced system to monitor account transactions but also add webhooks so data can be updated realtime (with some eventual consistency). To achieve the batch jobs, I'd look to utilise something such as Azure Scheduler and Functions to parallelise the requests as much as possible. I would try to make these requests as fast as possible and optimise for data transfer, later utilising something such as map-reduce to collaborate the data into meaningful statistics/locations. 

## Testing

My approach to testing generally is to ensure that I have solid behavioral tests in place. These can act both as documentation (how the system works), but also serve as regression tests to maintain the system when large scale refactoring/decoupling occur. I tend to approach a more linear testing strategy, rather than the traditional pyramid to strike a balance between maintainable code but ensure coverage on complex functionality.

I typically red-green-refactor; driving from Acceptance level initally which should remain red until all layers down are completed ie. Top Down development.

### Acceptance
- Drive out the journey's through the system (typically BDD style).

Ideally with the acceptance tests I would be mocking out the contracts (APIs) and pointing at/creating a database connection (possibly in memory) at runtime. This can be achieved through abstraction and dependancy injection. However this would need some modification to the Docker images, which I feel would take too long to implement right now, however once implemented would be very easy to replicate.

### Unit
- Assist code structure/reducing smells
- Help with self documenting code aka. Developer of tomorrow
- Provide fine grained testing to maintain the testing pyramid
- Quicker feedback loop of issues with complex functionality eg. Heavy maths problems.

### Integration
- Generally contractual testing of the outer extremes of the systems. Interactions with 3rd parties (such as the Banking Data), database interactions, etc

If I dedicated more time I would seperate the Acceptance Tests to use mocked databases and then use intergration tests to test database functionality.

## Future Considerations / Caveates

- How to handle when there is no existing account for the bank?
- Handling when the target bank is down?
- Adding more validation.