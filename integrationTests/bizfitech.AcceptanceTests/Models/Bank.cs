﻿namespace api.AcceptanceTests.Models
{
    using System;

    public class Bank
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
