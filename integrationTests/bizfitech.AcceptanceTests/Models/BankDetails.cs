namespace api.AcceptanceTests.Models
{
    using System;

    public class BankDetails
    {
        public Guid BankId { get; set; }
        string AccountNumber { get; }
    }
}