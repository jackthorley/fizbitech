namespace api.AcceptanceTests.Models
{
    using System;
    using Dapper.Contrib.Extensions;

    [Table("Users")]
    public class User
    {
        private User()
        {
        }

        [Key]
        public long Id { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }

        internal sealed class Builder
        {
            private User _user = new User();

            public Builder WithName(string firstname, string surname)
            {
                _user.Firstname = firstname;
                _user.Surname = surname;
                return this;
            }

            public User Build()
            {
                var user = _user;
                _user = null;

                return user;
            }
        }
    }
}