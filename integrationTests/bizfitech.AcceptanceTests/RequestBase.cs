namespace api.AcceptanceTests
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using api;
    using Helpers;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.TestHost;
    using Microsoft.Extensions.Configuration;

    public class RequestBase : IDisposable
    {
        public HttpClient httpClient { get; }

        public RequestBase()
        {
            var configuration = new ConfigurationBuilder().AddInMemoryCollection(new[]
                                                          {
                                                              new KeyValuePair<string, string>("Database:ConnectionString", Configuration.DatabaseConnectionString)
                                                          })
                                                          .Build();

            var databaseHelper = new DatabaseHelper(Configuration.DatabaseConnectionString);
            databaseHelper.ClearDatabase();

            var server = new TestServer(new WebHostBuilder()
                                            .UseConfiguration(configuration)
                                            .UseStartup<Startup>());

            httpClient = server.CreateClient();
        }



        public void Dispose()
        {
            httpClient.CancelPendingRequests();
        }
    }
}