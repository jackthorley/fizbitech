namespace api.AcceptanceTests.Users
{
    using System;
    using System.Net.Http;
    using Helpers;
    using Models;

    public class UserGetRequestBase : RequestBase, IDisposable
    {
        public User ExpectedUser { get; }
        public HttpResponseMessage Result { get; set; }


        public UserGetRequestBase()
        {
            ExpectedUser = new User.Builder().WithName("Test", "Person").Build();

            var databaseHelper = new DatabaseHelper(Configuration.DatabaseConnectionString);
            var userId = databaseHelper.InsertUser(ExpectedUser);
            ExpectedUser.Id = userId;

            Result = httpClient.GetAsync("/api/v1.0/users").Result;
        }

        public void Dispose()
        {
        }
    }
}