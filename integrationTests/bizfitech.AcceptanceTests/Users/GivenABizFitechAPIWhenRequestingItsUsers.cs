namespace api.AcceptanceTests.Users
{
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Xunit;

    public class GivenABizFitechAPIWhenRequestingItsUsers : IClassFixture<UserGetRequestBase>
    {
        private readonly UserGetRequestBase _baseFixture;

        public GivenABizFitechAPIWhenRequestingItsUsers(UserGetRequestBase baseFixture)
        {
            _baseFixture = baseFixture;
        }

        [Fact]
        public void ThenTheRequestWasSuccessful()
        {
            Assert.Equal(HttpStatusCode.OK, _baseFixture.Result.StatusCode);
        }

        [Fact]
        public void ThenTheUsersShouldBeReturned()
        {
            var users = _baseFixture.Result.Content.ReadAsAsync<Models.User[]>().Result;
            Assert.Equal(_baseFixture.ExpectedUser.Id, users[0].Id);
            Assert.Equal(_baseFixture.ExpectedUser.Firstname, users[0].Firstname);
            Assert.Equal(_baseFixture.ExpectedUser.Surname, users[0].Surname);
        }
    }  
}