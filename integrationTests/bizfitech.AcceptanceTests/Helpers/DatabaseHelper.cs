﻿namespace api.AcceptanceTests.Helpers
{
    using Models;
    using Dapper.Contrib.Extensions;
    using Microsoft.Data.Sqlite;

    internal class DatabaseHelper
    {
        private readonly string _connectionString;

        public DatabaseHelper(string connectionString)
        {
            _connectionString = connectionString;
        }
        
        public long InsertUser(User user)
        {
            using (var connection = new SqliteConnection(_connectionString))
            {
                connection.Open();
               return connection.Insert(user);
            }
        }

        public void ClearDatabase()
        {
            using (var connection = new SqliteConnection(_connectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "DELETE FROM UserBanks; DELETE FROM Users; DELETE FROM Banks;";
                command.ExecuteNonQuery();
            }
        }
    }
}
