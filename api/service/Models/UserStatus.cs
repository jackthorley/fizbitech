﻿namespace service.Models
{
    public class UserStatus : Enumeration
    {
        public string Message { get; }

        public static UserStatus Valid = new UserStatus(1, "Valid", "Valid");
        public static UserStatus DuplicateAccount = new UserStatus(2, "DuplicateAccount", "Duplicate Account");

        public UserStatus(int id, string name, string message) : base(id, name)
        {
            Message = message;
        }
    }
}