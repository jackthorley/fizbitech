﻿namespace service.Models
{
    public class DefaultStatus : Enumeration
    {
        public string Message { get; }

        public static DefaultStatus Valid = new DefaultStatus(1, "Valid", "Valid");
        public static DefaultStatus Invalid = new DefaultStatus(2, "Invalid", "Invalid");

        public DefaultStatus(int id, string name, string message) : base(id, name)
        {
            Message = message;
        }
    }
}