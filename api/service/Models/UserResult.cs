﻿namespace service.Models
{
    public class UserResult : ModelResult<User, UserStatus>
    {
        public UserResult(User result, UserStatus status) : base(result, status)
        {
        }
    }
}