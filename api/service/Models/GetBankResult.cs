﻿namespace service.Models
{
    public class GetBankResult : ModelResult<BankDetails, DefaultStatus>
    {
        public GetBankResult(BankDetails result, DefaultStatus status) : base(result, status)
        {
        }
    }
}