﻿namespace service.Models
{
    public class User
    {
        public long Id { get; private set; }
        public string Firstname { get; private set; }
        public string Surname { get; private set; }

        public User(long id, string firstname, string surname)
        {
            Id = id;
            Firstname = firstname;
            Surname = surname;
        }
    }
}
