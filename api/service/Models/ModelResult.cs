﻿namespace service.Models
{
    public class ModelResult<T, T1> where T : class
                                    where T1 : class
    {
        public T Result { get; }
        public T1 Status { get; }

        public ModelResult(T result, T1 status)
        {
            Result = result;
            Status = status;
        }
    }
}