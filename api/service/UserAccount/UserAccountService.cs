﻿namespace service.UserAccount
{
    using System.Threading.Tasks;
    using Models;

    internal class UserAccountService : IUserAccountService
    {
        public async Task<UserResult> AddUser(string firstname, string surname, string accountAccountNumber, long resultId)
        {
            //This request will review the UserBank table see if there's any pre-existing link between AccountId, User and BankId. Consistency of Account number seems like it's bank specific.
            return new UserResult(new Models.User(1, "asd", "asda"), UserStatus.Valid);
        }
    }
}