﻿namespace service.UserAccount
{
    using System.Threading.Tasks;
    using Models;

    public interface IUserAccountService
    {
        Task<UserResult> AddUser(string firstname, string surname, string accountAccountNumber, long resultId);
    }
}