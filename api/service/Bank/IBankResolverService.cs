﻿namespace service.Bank
{
    using System.Threading.Tasks;
    using Models;

    public interface IBankResolverService
    {
        Task<GetBankResult> GetBank(string bankName);
    }
}