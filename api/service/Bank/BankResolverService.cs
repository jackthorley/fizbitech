﻿namespace service.Bank
{
    using System.Threading.Tasks;
    using Models;

    internal class BankResolverService : IBankResolverService
    {
        public async Task<GetBankResult> GetBank(string bankName)
        {
            //I would add some sort of caching layer to this request. It's very unlikely to remove banks, if you do then you could downstream invalidate the cache. Highly cachable resource.
            return new GetBankResult(new BankDetails(), DefaultStatus.Valid);
        }
    }
}