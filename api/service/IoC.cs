﻿namespace service
{
    using Bank;
    using Data;
    using Data.Repository;
    using Microsoft.Extensions.DependencyInjection;
    using User;
    using UserAccount;

    public static class IoC
    {
        public static IServiceCollection AddServiceLayer(this IServiceCollection services)
        {
            services.AddTransient<IConnectionFactory, SQLiteConnectionFactory>();
            services.AddTransient<IUserGetService, UserGetService>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IBankResolverService, BankResolverService>();
            services.AddTransient<IUserAccountService, UserAccountService>();

            return services;
        }
    }
}
