﻿namespace service.User
{
    using System.Collections.Generic;
    using System.Linq;
    using Data.Repository;
    using Models;

    internal class UserGetService : IUserGetService
    {
        private readonly IUserRepository _userRepository;

        public UserGetService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public IEnumerable<User> Execute()
        {
            var userDtos = _userRepository.Get();
            return userDtos.Select(u => new User(u.Id, u.Firstname, u.Surname));
        }
    }
}