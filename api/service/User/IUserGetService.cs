﻿using System.Collections.Generic;

namespace service.User
{
    using Models;

    public interface IUserGetService
    {
        IEnumerable<User> Execute();
    }
}