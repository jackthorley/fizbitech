﻿namespace service.Data.Models
{
    using Dapper.Contrib.Extensions;

    [Table("Users")]
    internal class UserDto
    {
        public UserDto()
        {
        }

        [Key]
        public long Id { get; set; }

        public string Firstname { get; set; }
        public string Surname { get; set; }
    }
}