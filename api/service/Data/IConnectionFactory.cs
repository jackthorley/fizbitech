using System.Data;

namespace service.Data
{
    public interface IConnectionFactory
    {
        IDbConnection CreateConnection();
        
    }
}