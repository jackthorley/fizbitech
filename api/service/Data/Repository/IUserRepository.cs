namespace service.Data.Repository
{
    using System.Collections.Generic;
    using Models;

    internal interface IUserRepository
    {
        IEnumerable<UserDto> Get();
    }
}