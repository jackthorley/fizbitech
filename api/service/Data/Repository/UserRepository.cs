namespace service.Data.Repository
{
    using System.Collections.Generic;
    using Dapper;
    using Models;

    internal class UserRepository : IUserRepository
    {
        private readonly IConnectionFactory _connectionFactory;
        private string _sql;

        public UserRepository(IConnectionFactory connectionFactory)
        {
            _sql = "SELECT Id, Firstname, Surname FROM Users";
            _connectionFactory = connectionFactory;
        }

        public IEnumerable<UserDto> Get()
        {
            using (var conn = _connectionFactory.CreateConnection())
            {
                return conn.Query<UserDto>(_sql);
            }
        }
    }
}