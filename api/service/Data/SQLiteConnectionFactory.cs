﻿using System.Data;
using Microsoft.Extensions.Configuration;

namespace service.Data
{
    using Microsoft.Data.Sqlite;

    public class SQLiteConnectionFactory : IConnectionFactory
    {
        private readonly string _connectionString;

        public SQLiteConnectionFactory(IConfiguration configuration)
        {
            _connectionString = configuration["Database:ConnectionString"];
        }

        public IDbConnection CreateConnection()
        {
            return new SqliteConnection(_connectionString);
        }
    }
}