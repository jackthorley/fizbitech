﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Models;
    using service;
    using service.Bank;
    using service.Models;
    using service.User;
    using service.UserAccount;

    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class UsersController : ControllerBase
    {
        private readonly IUserGetService _userGetService;
        private readonly IBankResolverService _bankResolverService;
        private readonly IUserAccountService _userAccountService;

        public UsersController(IUserGetService userGetService, IBankResolverService bankResolverService, IUserAccountService userAccountService)
        {
            _userGetService = userGetService;
            _bankResolverService = bankResolverService;
            _userAccountService = userAccountService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<User>> Get()
        {
            var users = _userGetService.Execute();
            return new ActionResult<IEnumerable<User>>(users);
        }
        
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateUserRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest(new ErrorResponse(ModelState.Values.First().Errors.First().ErrorMessage));

            //Probably would wrap this up into a distinct service. Reduces complexity of controller layer
            var bankResult = await _bankResolverService.GetBank(request.Account.BankName);

            if (bankResult.Status != DefaultStatus.Valid)
                return BadRequest(new ErrorResponse("Invalid Bank"));

            var userResult = await _userAccountService.AddUser(request.Firstname, request.Surname, request.Account.AccountNumber, bankResult.Result.Id);
            //

            if (userResult.Status == UserStatus.Valid)
                return Created(new Uri($"api/users/{userResult.Result.Id}", UriKind.Relative), userResult.Result.Id);

            if (userResult.Status == UserStatus.DuplicateAccount)
                return BadRequest(new ErrorResponse(userResult.Status.Message));
            

            return BadRequest();
        }
    }
}
