﻿namespace api.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    public class Account
    {
        public string BankName { get; set; }

        [AccountNumberCheck]
        public string AccountNumber { get; set; }
    }

    public sealed class AccountNumberCheck : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string accountNumber = (string)validationContext.ObjectInstance;

            if (accountNumber.Length != 8 && accountNumber.ElementAt(0) == '0')
            {
                return new ValidationResult("Invalid Account Number");
            }

            return ValidationResult.Success;
        }

    }
}