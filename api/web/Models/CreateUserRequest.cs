﻿namespace api.Models
{
    public class CreateUserRequest
    {

        public string Firstname { get; set; }
        public string Surname { get; set; }
        public Account Account { get; set; }

    }
}