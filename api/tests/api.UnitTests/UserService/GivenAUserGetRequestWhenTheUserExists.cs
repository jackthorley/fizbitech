﻿namespace api.UnitTests.UserService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using service.Data.Models;
    using service.Data.Repository;
    using service.Models;
    using service.User;
    using Xunit;

    public class GivenAUserGetRequestWhenTheUserExists
    {
        private readonly Mock<IUserRepository> _userRepository;
        private readonly List<UserDto> _expectedUsers;
        private readonly IEnumerable<User> _users;

        public GivenAUserGetRequestWhenTheUserExists()
        {
            _userRepository = new Mock<IUserRepository>();

            _expectedUsers = new List<UserDto> {new UserDto {Firstname = "asdasd", Id = new Random().Next(), Surname = "asdasds"}};

            _userRepository.Setup(ur => ur.Get())
                           .Returns(_expectedUsers);


            var service = new UserGetService(_userRepository.Object);
            _users = service.Execute();
        }

        [Fact]
        public void ThenTheUserRepositoryIsCalled()
        {
            _userRepository.Verify(ur => ur.Get());
        }

        [Fact]
        public void ThenTheUsersAreReturned()
        {
            Assert.Equal(_expectedUsers.Count, _users.Count());
            Assert.Equal(_expectedUsers.ElementAt(0).Id, _users.ElementAt(0).Id);
        }

    }
}
